####################################################################
#  make include file.                                              #
####################################################################
#
SHELL = /bin/sh

# ----------------------------------------------------------------------
# - gpaw-cuda Directory Structure / gpaw-cuda library --------------------
# ----------------------------------------------------------------------
#
TOPdir       = .
INCdir       = $(TOPdir)
PYTHONINCdir?=/opt/programs/Python/2.7.11-intel-2015b/include/python2.7/
PYTHONLIBdir?=/opt/programs/Python/2.7.11-intel-2015b/lib/
NUMPYINCdir?=/opt/programs/Python/2.7.11-intel-2015b/lib/python2.7/site-packages/numpy/core/include
MPIINCdir?=/opt/programs/impi/5.0.3.048-iccifort-2015.3.187-GNU-4.9.3-2.25/include64
LIBdir       = $(TOPdir)
CUGPAWLIB    = $(LIBdir)/libgpaw-cuda.a 

#
# ----------------------------------------------------------------------
# - NVIDIA CUDA includes / libraries / specifics -----------------------
# ----------------------------------------------------------------------
CUDA_INSTALL_PATH=/opt/programs/CUDA/7.5.18-iccifort-2015.3.187-GNU-4.9.3-2.25
CUDAdir       = $(CUDA_INSTALL_PATH)
CUDAINCdir       = $(CUDAdir)/include
CUDALIBdir       = $(CUDAdir)/lib64
CUDA_OPTS     =

#
# ----------------------------------------------------------------------
# - gpaw-cuda includes / libraries / specifics -------------------------------
# ----------------------------------------------------------------------
#

CUGPAW_INCLUDES =  -I$(INCdir) -I$(CUDAINCdir) -I$(MPIINCdir) -I$(NUMPYINCdir)  -I$(PYTHONINCdir)
CUGPAW_OPTS     = -DPARALLEL=1 -DGPAW_CUDA=1
#CUGPAW_OPTS     = -DPARALLEL=1 -DGPAW_CUDA=1 -DMPI_CUDA=1

#
# ----------------------------------------------------------------------
#

CUGPAW_DEFS     = $(CUGPAW_OPTS) $(CUDA_OPTS)  $(CUGPAW_INCLUDES)

#
# ----------------------------------------------------------------------
# - Compilers / linkers - Optimization flags ---------------------------
# ----------------------------------------------------------------------

CC           = icc
CCNOOPT      = $(CUGPAW_DEFS)
# CCFLAGS      = $(CUGPAW_DEFS) -fomit-frame-pointer -mfpmath=sse -msse3 -mtune=native -O3 -funroll-loops -W -Wall -m64 -fPIC -std=c99
CCFLAGS      = $(CUGPAW_DEFS) -g -fPIC -std=c99 -m64 -O3

NVCC           = nvcc_icpc
NVCCFLAGS      = $(CUGPAW_DEFS) -O3 -g -arch=sm_20 -m64 --compiler-options '-O3 -g -std=c99 -fPIC' 
#NVCCFLAGS      = $(CUGPAW_DEFS) -Xptxas -v -O3 -g -arch=sm_20 -m64 --compiler-options '-O3 -g -std=c99 -fPIC' 
#NVCCFLAGS      = $(CUGPAW_DEFS) -Xptxas -v -O3 -g -G -arch=sm_20 -m64 --compiler-options '-O3 -g -std=c99 -fPIC' 



ARCH     = ar
ARCHFLAGS= cr
RANLIB   = ranlib


