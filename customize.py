#User provided customizations for the gpaw setup

#Here, one can override the default arguments, or append own
#arguments to default ones
#To override use the form
#     libraries = ['somelib','otherlib']
#To append use the form
#     libraries += ['somelib','otherlib']

# Valid values for scalapack are False, or True:
# False (the default) - no ScaLapack compiled in
# True - ScaLapack compiled in
#scalapack = True

#compiler = 'mpcc'
#libraries = []
#libraries += []

#library_dirs = []
#library_dirs += []

#include_dirs = []
#include_dirs += []

#extra_link_args = []
#extra_link_args += []

#extra_compile_args = []
#extra_compile_args += []

#runtime_library_dirs = []
#runtime_library_dirs += []

#extra_objects = []
#extra_objects += []

#define_macros = []
#define_macros += []

#mpicompiler = None
#mpilinker = None
#mpi_libraries = []
#mpi_libraries += []

#mpi_library_dirs = []
#mpi_library_dirs += []

#mpi_include_dirs = []
#mpi_include_dirs += []

#mpi_runtime_library_dirs = []
#mpi_runtime_library_dirs += []

#mpi_define_macros = []
#mpi_define_macros += []

#platform_id = ''
libraries.remove("blas")
libraries.remove("lapack")
compiler = 'icc'
mpicompiler = 'mpiicc'  # use None if you don't want to build a gpaw-python
mpilinker = 'mpiicc'
# platform_id = ''
scalapack = True
hdf5 = True
define_macros += [('GPAW_NO_UNDERSCORE_CBLACS', '1')]
define_macros += [('GPAW_NO_UNDERSCORE_CSCALAPACK', '1')]
define_macros += [('GPAW_CUDA', '1')]
define_macros += [('GPAW_NO_UNDERSCORE_CBLACS', '1')]
define_macros += [('GPAW_NO_UNDERSCORE_CSCALAPACK', '1')]
define_macros += [("GPAW_ASYNC",1)]
define_macros += [("GPAW_MPI2",1)]
libraries = [
	'xc',
	'gpaw-cuda',
	'cublas',
	'cudart',
	'stdc++',
	'mpi',
	'mkl_scalapack_lp64',
	'mkl_intel_lp64',
	'mkl_core',
	'mkl_sequential',
	'mkl_blacs_intelmpi_lp64',
	'hdf5_hl',
	'hdf5',
	'hdf5hl_fortran',
	'z',
	'readline',

]
library_dirs += [
	'/opt/programs/imkl/11.3.1.150-iimpi-8.1.5-GCC-4.9.3-2.25/compilers_and_libraries_2016.1.150/linux/compiler/lib/intel64_lin',
	'./c/cuda',
	'/opt/programs/HDF5/1.8.16-intel-2015b/lib',
	'/opt/programs/libreadline/6.3-intel-2015b/lib',
	'/opt/programs/CUDA/7.5.18-iccifort-2015.3.187-GNU-4.9.3-2.25/lib64',
	'/opt/programs/libxc/3.0.0-intel-2015b/lib'
]
include_dirs +=[
	'/opt/programs/CUDA/7.5.18-iccifort-2015.3.187-GNU-4.9.3-2.25/include',
	'/opt/programs/libxc/3.0.0-intel-2015b/include'
]
