.. _tutorials:

=========
Tutorials
=========

If you are not familiar with :ase:`ASE <>`, consider
:ase:`ASE Tutorials <tutorials/tutorials.html>` first.

The GPAW tutorials are meant to guide the user through some scripts to
perform some of the most common tasks:

.. toctree::
   :maxdepth: 2

   lattice_constants/lattice_constants
   atomization/atomization
   atomization_cmr/atomization_cmr
   optimization/optimization
   plotting/plot_wave_functions
   bandstructures/bandstructures
   band_gap/band_gap
   dipole_correction/dipole
   jellium/jellium
   hydrogen/h
   all-electron/all_electron_density
   neb/neb
   pbe0/pbe0
   xas/xas
   negfstm/negfstm
   dielectric_response/dielectric_response
   rpa/rpa_tut
   ensembles/ensembles
   gw_tutorial/gw_tutorial


=========
Exercises
=========

Please follow :ref:`exercises`.
