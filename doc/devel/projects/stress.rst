Calculation of stress tensor
============================

:Who:
    Ask and Ivano

The plan is to implement this in the plane-wave part of the code,
where all the terms are relatively simple.  So far, we have coded the
kinetic energy contribution.
