Distributed version control system
==================================

:Who:
    Jens Jørgen

Move the GPAW code from our local centralized version control system (SVN)
to bazaar_ on launchpad_.

.. _bazaar: http://bazaar.canonical.com/en/
.. _launchpad: https://launchpad.net/
