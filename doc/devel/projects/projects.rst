================
Ongoing Projects
================


.. toctree::

    setups
    rmmdiis
    mixing
    pw
    stress
    response
    bse
    gw
    gwtransport
    ehrenfest
    beef
    qmmm
    sic
    lrtddft
    gpu
    noncollinear
    hybrids
    dvcs
    pcm
    elph
